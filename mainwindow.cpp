#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtMath>
#include <QMessageBox>
#include <QFileDialog>
#include <QCryptographicHash>
#include <QDebug>

#include "FormAbout.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),    ui(new Ui::MainWindow){
    ui->setupUi(this);

    // Created: 23.08.2018

    this->setFixedSize(this->width(),this->height());
    this->setWindowTitle("Serial Key Generator v1.0.0");


    ui->label_12->setPixmap(QPixmap(":data/background_wait.png"));
    ui->label_12->setGeometry(0,0,this->width(),this->height());
    ui->label_12->hide();


    widgetProgressBar = new FormProgressBar(this);
    widgetProgressBar->move((this->width()-widgetProgressBar->width())/2,(this->height()-widgetProgressBar->height())/2);
    widgetProgressBar->hide();

    thGenerateSerials = new TheadGenerateSerials();
    thGenerateSerials->start();


    ui->lineEdit->setText(myRnd(8,2));

    ui->groupBox_2->setEnabled(false);


    refreshSample();

    connect(thGenerateSerials,SIGNAL(generateFinish(int)),this,SLOT(generateFinish(int)));
    connect(thGenerateSerials,SIGNAL(status(int,int)),this,SLOT(generatorStatus(int,int)));

    connect(ui->spinBox,SIGNAL(valueChanged(int)),this,SLOT(refreshSample()));
    connect(ui->spinBox_2,SIGNAL(valueChanged(int)),this,SLOT(refreshSample()));
    connect(ui->comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(refreshSample()));

    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(close()));
    connect(ui->pushButton_2,SIGNAL(clicked(bool)),this,SLOT(generate()));
    connect(ui->pushButton_3,SIGNAL(clicked(bool)),this,SLOT(about()));

    connect(ui->pushButton_5,SIGNAL(clicked(bool)),this,SLOT(exportPlainSerial()));
    connect(ui->pushButton_4,SIGNAL(clicked(bool)),this,SLOT(exportCryptSerial()));
}

MainWindow::~MainWindow(){
    delete thGenerateSerials;
    delete ui;
}


// Обновить label примера
void MainWindow::refreshSample(){
    int columns = ui->spinBox->value();
    int chars = ui->spinBox_2->value();

    QString text_sample="";

    for (int i=0;i<columns;i++) text_sample.append(myRnd(chars,ui->comboBox->currentIndex())+"-");
    text_sample.chop(1);

    ui->label_5->setText(text_sample);
}

// Начать генерацию серийных номеров
void MainWindow::generate(){
    int columns = ui->spinBox->value();
    int chars = ui->spinBox_2->value();
    qreal countSerials = ui->spinBox_3->value();
    qreal maxSerials = qPow(36,columns*chars);


    if (countSerials>maxSerials){
        QMessageBox::critical(this,tr("Serial Key Generator"),tr("Invalid number of serial numbers"));
        return;
    }

    ui->label_12->show();
    widgetProgressBar->setProgressBarMinMax(0,countSerials);
    widgetProgressBar->setProgressBarValue(0);
    widgetProgressBar->show();
    thGenerateSerials->startGenerate(ui->spinBox_3->value(), columns,chars,ui->comboBox->currentIndex());

}

void MainWindow::about(){
    FormAbout form;
    form.exec();
}

// Экпортировать в файл
void MainWindow::exportToFile(bool is_crypt){
    int export_format = ui->comboBox_2->currentIndex();
    bool export_with_separator = ui->checkBox->isChecked();
    QString salt = ui->lineEdit->text();

    QString filename="";

    if (export_format==0 or export_format==1){
        filename = QFileDialog::getSaveFileName(this,tr("Export to file"),"","Text (*.txt)");
    }
    if (export_format==2){
        filename = QFileDialog::getSaveFileName(this,tr("Export to file"),"","SQL (*.sql)");
    }
    if (filename == "") return;

    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        QMessageBox::critical(this,tr("Error open file"),file.errorString(),QMessageBox::Ok);
        return;
    }
    QTextStream out(&file);

    QMapIterator<QString, int> i(listSerials);
    while (i.hasNext()) {
        i.next();
        QString key = i.key();

        if (export_with_separator==false){
            key.replace("-","");
        }
        if (is_crypt){
            key = getMD5(key+salt);
        }

        switch (export_format) {
        case 0:
            out << key << "\n";
            break;
        case 1:
            out << key << ";";
            break;
        case 2:
            out << "INSERT INTO `serials` (`serial`) VALUES (\""+key+"\")" << "\n";
            break;
        default:
            break;
        }
    }

    file.close();

}

// Экспорт чистых серийных номеров
void MainWindow::exportPlainSerial(){
    exportToFile(false);
}

// Экспорт хешей серийных номеров
void MainWindow::exportCryptSerial(){
    exportToFile(true);
}

// слот завершения генерации
void MainWindow::generateFinish(int code){
    listSerials = thGenerateSerials->getListSerials();
    ui->label_8->setText(QString::number(listSerials.size()));
    widgetProgressBar->hide();
    ui->label_12->hide();
    ui->groupBox_2->setEnabled(true);
}

// Статус генерации серийных номеров
void MainWindow::generatorStatus(int current, int max){
    widgetProgressBar->setProgressBarValue(current);
}

// Генерация случайной строки
QString MainWindow::myRnd(int length,int type){
    QString result="";
    int s=0;

    QString abs="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
    if (type==0) abs="1234567890QWERTYUIOPASDFGHJKLZXCVBNM";
    if (type==1) abs="1234567890qwertyuiopasdfghjklzxcvbnm";

    for (int i=0;i<length;i++){
        s=qrand()%abs.length();
        result+=abs.mid(s,1);
    }

    return result;
}

// Получение md5 строки
QString MainWindow::getMD5(QString key){
    QByteArray ba = key.toUtf8();
    return QString(QCryptographicHash::hash(ba, QCryptographicHash::Md5).toHex());
}
