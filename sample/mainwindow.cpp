#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QMessageBox>
#include <QCryptographicHash>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setFixedSize(this->width(),this->height());
    this->setWindowTitle("Sample check serial");


    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(close()));
    connect(ui->pushButton_2,SIGNAL(clicked(bool)),this,SLOT(check_serial()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::check_serial(){
    QString serial = ui->lineEdit->text();
    QString salt = "gXV16UDC";

    QByteArray ba = (serial+salt).toUtf8();
    QByteArray serial_hash = QCryptographicHash::hash(ba, QCryptographicHash::Md5).toHex();

    bool serialValid = false;

    QFile file("hash.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QMessageBox::critical(this,"Sample check serial","hash.txt not found");
        return;
    }
    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        line.replace("\n","");
        if (serial_hash==line){
            serialValid=true;
            break;
        }
    }
    file.close();

    if (serialValid){
        QMessageBox::information(this,"Sample check serial","Serial valid!");
    }else{
        QMessageBox::critical(this,"Sample check serial","Serial not valid!");
    }

}
