#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>

#include "TheadGenerateSerials.h"
#include "FormProgressBar.h"


namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    private slots:
        void refreshSample();
        void generate();
        void about();

        void exportPlainSerial();
        void exportCryptSerial();

        void generateFinish(int code);
        void generatorStatus(int current,int max);

    private:
        Ui::MainWindow *ui;

        void exportToFile(bool is_crypt);
        QString myRnd(int length, int type);
        QString getMD5(QString key);

        TheadGenerateSerials *thGenerateSerials;
        FormProgressBar *widgetProgressBar;

        QMap<QString,int> listSerials;
};

#endif // MAINWINDOW_H
