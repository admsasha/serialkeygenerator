#ifndef THEADGENERATESERIALS_H
#define THEADGENERATESERIALS_H

#include <QThread>
#include <QMap>
#include <QMutex>
#include <QWaitCondition>

class TheadGenerateSerials : public QThread {
    Q_OBJECT
    public:
        TheadGenerateSerials();
        void run();

        void startGenerate(int counts, int columns, int chars, int type);
        QMap<QString,int> getListSerials();

    signals:
        void generateFinish(int code);
        void status(int current, int max);

    private:
        QString myRnd(int length, int type);

        QMap<QString,int> listSerials;

        QWaitCondition wait;
        QMutex lock;

        bool _running;

        int _counts;
        int _columns;
        int _chars;
        int _type;
};

#endif // THEADGENERATESERIALS_H
