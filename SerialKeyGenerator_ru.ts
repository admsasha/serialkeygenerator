<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>FormProgressBar</name>
    <message>
        <location filename="FormProgressBar.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="FormProgressBar.ui" line="31"/>
        <source>Serial numbers are being generated</source>
        <translation>Создаются серийные номера</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="27"/>
        <source>Exit</source>
        <translation type="unfinished">Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40"/>
        <source>Generate serials</source>
        <translation type="unfinished">Генерация серийных номеров</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="52"/>
        <source>Generate</source>
        <translation>Генерировать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="66"/>
        <source>Only upper</source>
        <translation>Только верхний</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="71"/>
        <source>Only lower</source>
        <translation>Только нижний</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <source>Upper + lower</source>
        <translation>Верхний + нижний</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="95"/>
        <source>Chars/column:</source>
        <translation>Символов/колонку:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="113"/>
        <source>Counts:</source>
        <translation>Количество:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="131"/>
        <source>Columns:</source>
        <translation>Колонок</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="149"/>
        <source>Case:</source>
        <translation>Регистр:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="218"/>
        <source>XXXX-XXXX-XXXX-XXXX-XXXX-XXXX-XXXX-XXXX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="236"/>
        <source>Sample:</source>
        <translation>Пример:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <source>Export</source>
        <translation>Экспорт</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="267"/>
        <source>Counts serial:</source>
        <translation>Кол-во номеров:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="285"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="298"/>
        <source>Export hash</source>
        <translation type="unfinished">Экспорт хешей</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="311"/>
        <source>Salt:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="334"/>
        <source>Export plain</source>
        <translation>Простой экспорт</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="347"/>
        <source>output: md5(serial+salt)</source>
        <translation>Вывод: md5(serial+salt)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="361"/>
        <source>Each on a new line</source>
        <translation>Каждый на новой строке</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <source>One line through a separator</source>
        <translation>В одну строку с разделителем</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="371"/>
        <source>SQL query insert</source>
        <translation>SQL запрос вставки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="385"/>
        <source>Format export:</source>
        <translation>Формат экспорта</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <source>export with separator columns (-)</source>
        <translation>экспорт с разделителем колонок (-)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="415"/>
        <source>background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="77"/>
        <source>Serial Key Generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="77"/>
        <source>Invalid number of serial numbers</source>
        <translation>Выбрано недопустимое колицество номеров</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="98"/>
        <location filename="mainwindow.cpp" line="101"/>
        <source>Export to file</source>
        <translation>Экспорт в файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="107"/>
        <source>Error open file</source>
        <translation>Ошибка открытия файла</translation>
    </message>
</context>
</TS>
