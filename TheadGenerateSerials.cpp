#include "TheadGenerateSerials.h"
#include <QDebug>

TheadGenerateSerials::TheadGenerateSerials(){
    _running=false;
}

void TheadGenerateSerials::run(){

    QString key;

    while (1) {
        lock.lock();
        wait.wait(&lock,1000);
        if (_running==false){
            lock.unlock();
            continue;
        }
        lock.unlock();


        listSerials.clear();
        int codeExit = 0;

        while(1){
            key.clear();
            for (int i=0;i<_columns;i++) key.append(myRnd(_chars,_type)+"-");
            key.chop(1);

            if (listSerials.contains(key)==false){
                listSerials[key]=1;
                emit status(listSerials.size(),_counts);
            }

            if (listSerials.size()>=_counts) break;
        }

        _running=false;
        emit generateFinish(codeExit);
    }
}

void TheadGenerateSerials::startGenerate(int counts, int columns, int chars, int type){
    _counts=counts;
    _columns=columns;
    _chars=chars;
    _type=type;
    _running=true;

    wait.wakeAll();
}

QMap<QString, int> TheadGenerateSerials::getListSerials(){
    return listSerials;
}



QString TheadGenerateSerials::myRnd(int length,int type){
    QString result="";
    int s=0;

    QString abs="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
    if (type==0) abs="1234567890QWERTYUIOPASDFGHJKLZXCVBNM";
    if (type==1) abs="1234567890qwertyuiopasdfghjklzxcvbnm";

    for (int i=0;i<length;i++){
        s=qrand()%abs.length();
        result+=abs.mid(s,1);
    }

    return result;
}
