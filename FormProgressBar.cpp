#include "FormProgressBar.h"
#include "ui_FormProgressBar.h"

FormProgressBar::FormProgressBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormProgressBar)
{
    ui->setupUi(this);
}

FormProgressBar::~FormProgressBar(){
    delete ui;
}

void FormProgressBar::setProgressBarMinMax(int min, int max){
    ui->progressBar->setMinimum(min);
    ui->progressBar->setMaximum(max);
}

void FormProgressBar::setProgressBarValue(int value){
    ui->progressBar->setValue(value);
}
