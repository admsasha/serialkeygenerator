#ifndef FORMPROGRESSBAR_H
#define FORMPROGRESSBAR_H

#include <QWidget>

namespace Ui {
class FormProgressBar;
}

class FormProgressBar : public QWidget
{
    Q_OBJECT

    public:
        explicit FormProgressBar(QWidget *parent = nullptr);
        ~FormProgressBar();

        void setProgressBarMinMax(int min,int max);
        void setProgressBarValue(int value);

    private:
        Ui::FormProgressBar *ui;
};

#endif // FORMPROGRESSBAR_H
