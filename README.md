# Serial Key Generator #

Is application created for software developers who want to protect their applications by serial key registration.

![SerialKeyGenerator images](http://dansoft.krasnokamensk.ru/data/1025/SerialKeyGenerator.png)


### How do I get set up? ###
qmake

lupdate ./SerialKeyGenerator.pro

lrelease ./SerialKeyGenerator.pro

mkdir /usr/share/SerialKeyGenerator/

cp *.qm /usr/share/SerialKeyGenerator/

make

### Who do I talk to? ###
email: dik@inbox.ru
